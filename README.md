### Hiring - HTML/CSS Test

We build embeddable tools via the Web Components standard to deliver financial and credit services to our clients.

The first step in building a new consumer-facing Web Component is to use the HTML/CSS and other assets created by our designers and web slicing team. We use Figma to design our pages/components and then have web slicers create the HTML/CSS. From there, our frontend development team creates Web Components and React/Next.js applications.

##### The objectives are:

- Open this repo in a new Gitpod workspace by clicking here: [![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/array.com/tests-html-css)
- Create a Figma account if you don't already have one.
- Import the `test.fig` Figma file of one of our web components called ScoreSight.
- Create an HTML page called `array-score-sight.html` where you slice the Figma file into pixel-perfect HTML/CSS.
- To preview your work, right click on `array-score-sight.html` and left click on "Open with Live Server"
- You only need to focus on the "3B - Results" desktop and mobile views.
- The resulting HTML page should be responsive and ready for a frontend engineer to start adding interactivity and API functionality.
- Do not use JQuery, Zepto, or any external javascript frameworks. Only external CSS that can be hosted alongside the HTML page is acceptable.

##### When complete:

Please write a `README` that includes how to start your application and anything we should know. In addition, please include notes on any design choices you made throughout the exercise (dependencies, libraries, assumptions, etc.).

Additionally, please create an unlisted YouTube video on the final deliverable of your test. Include any information on how you approached your test, what choices you made, what you liked, disliked, etc. 

Open https://gitpod.io/workspaces and click the workspace menu icon and enable "Share" so we can access your Gitpod workspace.

Submit the link for your Gitpod workspace and video to: https://docs.google.com/forms/d/e/1FAIpQLSfWYuJ4T1OZ1Bmc77fMJCe3X21AzOKhfdvAobWzupSeR3U7CA/viewform

We will review your submission within two business days!